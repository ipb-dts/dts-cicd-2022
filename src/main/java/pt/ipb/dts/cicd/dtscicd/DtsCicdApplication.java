package pt.ipb.dts.cicd.dtscicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Paths;

@SpringBootApplication
public class DtsCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtsCicdApplication.class, args);
	}

}
