# Environment Setup

We recommend using [IntelliJ](https://www.jetbrains.com/idea).
You can apply for a free renewable 1-year license for Ultimate Edition [here](https://www.jetbrains.com/community/education/#students) (hit the Apply button).

# Spring Download

* https://start.spring.io
  * __Configuration__:
    * Gradle Project
    * Java
    * 2.7.5
    * `pt.ipb.dts.cicd:dts-cicd-2022` 
    * Jar
    * 17
  * __Dependencies__:
    * Spring Data JPA
    * Spring Web
    * Spring Boot DevTools
  * Generate (⌘ + ⏎)
  * Unzip and open the project

# Application

Open `build.gradle` and (somewhere below line `18`) add:
```groovy
runtimeOnly 'com.h2database:h2' // embedded database engine
```
 
Open `src/main/resources/application.properties` and add:
```properties
server.port=7070
server.servlet.context-path=/
#
spring.datasource.url=jdbc:h2:file:///tmp/simple-app/db
spring.datasource.username=sa
spring.datasource.password=sa123
spring.h2.console.path=/h2
spring.h2.console.enabled=true
#
spring.jpa.hibernate.ddl-auto=update
```

Delete all test classes under `src/test/java` _(unit testing is out of the scope of this demo)_

Build and start the Application, head to http://127.0.0.1/h2, should be able to login with:

- **JDBC URL**: `jdbc:h2:file:///tmp/simple-app/db`
- **User Name**: `sa`
- **Password**: `sa123`

#### Implement the `Note` (entity), `NoteRepository` and `NoteController` classes

#### Add a minimal `index.html` to `src/main/resources/static`

# Windows Setup

1. Download and Install [docker desktop](https://www.docker.com/products/docker-desktop/).
2. First start will fail and suggest going to - https://aka.ms/wsl2kernel - proceed and follow the instructions.
   - After setting up, install [Debian WSL2 from the miscrosoft store](https://www.microsoft.com/store/productId/9MSVKQC78PK6). 

# GitLab Runner

1. Create a GitLab Project to host the project.
2. From `Settings > CI/CD > Runners` copy the token for your project (use it in `<project-token>` below).
3. Download the binaries [here](https://docs.gitlab.com/runner/install/#binaries).
   - If using windows just move it to a folder you remember (ex.: `/dev/gitlab-runner`) after download.
4. Execute (as administrator):
```shell
# register and install
cd /dev/gitlab-runner # if you are using windows

# runner for building and packaging
gitlab-runner register ^
  --registration-token <project-token> ^
  --non-interactive ^
  --url https://gitlab.com ^
  --tag-list dts,deploy ^
  --name ipb-dts-deploy-runner ^
  --executor docker ^
  --docker-image alpine:latest ^
  --docker-volumes /dev/deploy:/deploy ^
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

# runner to actually deploy
gitlab-runner register ^
  --registration-token <project-token> ^
  --non-interactive ^
  --url https://gitlab.com ^
  --tag-list dts,shell ^
  --name ipb-dts-shell-runner ^
  --executor shell ^
  --shell powershell
  
gitlab-runner install
gitlab-runner start
```

If you need to revert it:
```shell
# reverting
gitlab-runner.exe unregister --all-runners
gitlab-runner.exe stop
gitlab-runner.exe uninstall
```

# CI Variables

- https://docs.gitlab.com/ee/ci/variables/

# Build Docker Image

```shell
docker build -t dts-cicd-demo --build-arg JAR_FILE=build/libs/dts-cicd-demo-0.0.1-SNAPSHOT.jar .
```
